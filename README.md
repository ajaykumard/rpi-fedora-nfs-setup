# NFS Setup

## Fedora
```bash
sudo service nfs status
sudo service nfs enable // ? use systemctl
sudo service nfs start
```

## RPi
```bash
sudo apt-get update
sudo apt-get upgrade
sudo apt-get dist-upgrade
```

install nfs
```bash
sudo apt-get install nfs-common nfs-server
```

In `/etc/exports` add
```
/srv/nfs4/share 192.168.CCC.DDD(rw,sync,no_subtree_check)
```

Restart nfs kernel sever
```bash
sudo /etc/init.d/nfs-kernel-server restart
```

